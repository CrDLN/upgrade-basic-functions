//Crea una función que reciba por parámetro un array y compruebe si existen elementos duplicados,
//en caso que existan los elimina para retornar un array sin los elementos duplicados. Puedes usar este array para probar tu función:

const duplicates = [
    'sushi',
    'pizza',
    'burger',
    'potatoe',
    'pasta',
    'ice-cream',
    'pizza',
    'chicken',
    'onion rings',
    'pasta',
    'soda'
  ];
  function removeDuplicates(param) {
    // insert code
    let auxArray = duplicates;
    for (let i = 0; i < param.length; i++) {
        
        let contD=0;
        auxArray.forEach(element => {
            if(param[i]===element){
                contD=contD+1;
                if(contD===2){
                    param.splice(i,1);
                }
            }
        });
    }

    return param;
  }
//hago el borrado al revés
  console.log(removeDuplicates(duplicates));