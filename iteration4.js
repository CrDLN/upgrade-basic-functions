//Calcular un promedio es una tarea extremadamente común. Puedes usar este array para probar tu función:

const numbers = [12, 21, 38, 5, 45, 37, 6];
function average(param) {
    let sumTotal=0;
    param.forEach(element => {
      sumTotal=sumTotal+element;
  });
  return sumTotal/param.length;
}


console.log(average(numbers));