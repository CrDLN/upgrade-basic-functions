//Crea una función que reciba por parámetro un array y cuando es un valor number lo sume y de lo
//contrario cuente la longitud del string y lo sume. Puedes usar este array para probar tu función:
const mixedElements = [6, 1, 'Rayo', 1, 'vallecano', '10', 'upgrade', 8, 'hub'];
function averageWord(param) {
  // insert code
  let aux=0;
  param.forEach(element => {
      
      if(typeof element == 'string'){
          aux= aux + element.length;
      }else{
        aux= aux + element;
      }
  });

  return aux;
  
}

console.log(averageWord(mixedElements));